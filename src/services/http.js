import Axios from 'axios'

export const Http = Axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    XMLHttpRequest: 'XMLHttpRequest'
  }
})
