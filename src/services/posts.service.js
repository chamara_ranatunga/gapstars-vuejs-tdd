
import { Http } from './http.js'

export const PostsService = {

  /**
     * Get all post list
     */
  list () {
    return Http.get('/posts')
  }

}
