import { shallowMount } from '@vue/test-utils'
import ActionList from '@/components/ActionList.vue'
import getTimeTravelPropsData from './utils/action-props'

describe('Action list component unit testing', () => {
  it('is a Vue instance', () => {
    const wrapper = shallowMount(ActionList, getTimeTravelPropsData())
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders check post list count', () => {
    const wrapper = shallowMount(ActionList, getTimeTravelPropsData())
    const posts = wrapper.findAll('#action')
    expect(posts.length).toBe(2)
  })

  it('check the move up call', () => {
    const wrapper = shallowMount(ActionList, getTimeTravelPropsData())
    const actionRevert = jest.fn()

    wrapper.setMethods({
      actionRevert: actionRevert
    })

    wrapper.find('.button0').trigger('click')
    expect(actionRevert).toHaveBeenCalled()
  })
})
