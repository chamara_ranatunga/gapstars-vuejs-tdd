import { shallowMount } from '@vue/test-utils'
import PostList from '@/components/PostList.vue'
import getPostsPropsData from './utils/posts-props'

describe('Post list component unit testing', () => {
  it('is a Vue instance', () => {
    const wrapper = shallowMount(PostList, getPostsPropsData())
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders check post list count', () => {
    const wrapper = shallowMount(PostList, getPostsPropsData())
    const posts = wrapper.findAll('#posts')
    expect(posts.length).toBe(5)
  })

  it('check the move up call', () => {
    const wrapper = shallowMount(PostList, getPostsPropsData())
    const moveUp = jest.fn()

    wrapper.setMethods({
      moveUp: moveUp
    })

    wrapper.find('.moveUp1').trigger('click')

    expect(moveUp).toHaveBeenCalled()
  })

  it('check the move down call', () => {
    const wrapper = shallowMount(PostList, getPostsPropsData())
    const moveDown = jest.fn()

    wrapper.setMethods({
      moveDown: moveDown
    })

    wrapper.find('.moveDown1').trigger('click')
    expect(moveDown).toHaveBeenCalled()
  })
})
