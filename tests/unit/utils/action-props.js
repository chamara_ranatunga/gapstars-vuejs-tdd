/**
 * Test utils functions for action list
 */

export default function getTimeTravelPropsData () {
  return {
    propsData: {
      timeTravelList: [
        {
          id: 1,
          old_index: 0,
          new_index: 1
        },
        {
          id: '2',
          old_index: 1,
          new_index: 2
        }

      ],
      title: 'List of action committed'
    }
  }
}
