/**
 * Test utils functions for posts list
 */

export default function getPostsPropsData () {
  return {
    propsData: {
      posts: [
        {
          id: 1,
          title: 'post 1'
        },
        {
          id: '2',
          title: 'post 2'
        },
        {
          id: 3,
          title: 'post 3'
        },
        {
          id: 4,
          title: 'post 4'
        },
        {
          id: 5,
          title: 'post 5'
        }
      ],
      limit: 5,
      title: 'Sortable List'
    }
  }
}
